# Curso de React Native by Stephen Grider

**Link al curso:**
https://www.udemy.com/course/the-complete-react-native-and-redux-course

Dentro de este repositorio se planea el respaldo y documentación concreta sobre las actividades vistas en el curso antes mencionado.
La finalidad de este repositorio es organizar lo visto en cada sección por puro desarrollo personal (por si algo se me llega a olvidar :p)

_Keep Learning!_
